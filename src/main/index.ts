import * as path from 'path'
import { app, BrowserWindow } from 'electron'

const allowedModules = new Set()
const allowedElectronModules = new Set(['dialog'])
const allowedGlobals = new Set()
function createWindow() {
  const win = new BrowserWindow({
    minWidth: 900,
    minHeight: 600,
    width: 900,
    height: 600,
    /* frame: false, */
    autoHideMenuBar: true,
    hasShadow: true,
    webPreferences: {
      preload: path.join(app.getAppPath(), 'preload.js'),
      contextIsolation: true
    }
  })
  win.webContents.openDevTools({ mode: 'detach' })
  if (process.env.NODE_ENV === 'development') {
    win.loadURL('http://localhost:8083')
  } else {
    if (isHandleSquirrelEvent()) {
      win.loadFile('./render/index.html', { hash: '/setup' })
    } else {
      win.loadFile('./render/index.html')
    }
  }
}
app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  /* if (process.platform !== 'darwin') {
    app.quit()
  } */
  app.quit()
})

app.on('remote-require', (event, webContents, moduleName) => {
  if (!allowedModules.has(moduleName)) {
    event.preventDefault()
  }
})

app.on('remote-get-builtin', (event, webContents, moduleName) => {
  if (!allowedElectronModules.has(moduleName)) {
    event.preventDefault()
  }
})

app.on('remote-get-global', (event, webContents, globalName) => {
  if (!allowedGlobals.has(globalName)) {
    event.preventDefault()
  }
})

app.on('remote-get-current-window', (event, webContents) => {
  event.preventDefault()
})

app.on('remote-get-current-web-contents', (event, webContents) => {
  event.preventDefault()
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

// this should be placed at top of main.js to handle setup events quickly
handleSquirrelEvent()
function isHandleSquirrelEvent() {
  if (process.argv.length === 1) {
    return false
  }
  return true
}
function handleSquirrelEvent() {
  if (process.argv.length === 1) {
    return false
  }

  const ChildProcess = require('child_process')
  const path = require('path')

  const appFolder = path.resolve(process.execPath, '..')
  const rootAtomFolder = path.resolve(appFolder, '..')
  const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'))
  const exeName = path.basename(process.execPath)

  const spawn = function (command, args) {
    return ChildProcess.spawn(command, args, { detached: true })
  }

  const spawnUpdate = function (args) {
    return spawn(updateDotExe, args)
  }

  const squirrelEvent = process.argv[1]
  switch (squirrelEvent) {
    case '--squirrel-install':
    case '--squirrel-updated':
      // Optionally do things such as:
      // - Add your .exe to the PATH
      // - Write to the registry for things like file associations and
      //   explorer context menus

      // Install desktop and start menu shortcuts
      spawnUpdate(['--createShortcut', exeName])

      setTimeout(app.quit, 1000)
      return true

    case '--squirrel-uninstall':
      // Undo anything you did in the --squirrel-install and
      // --squirrel-updated handlers

      // Remove desktop and start menu shortcuts
      spawnUpdate(['--removeShortcut', exeName])

      setTimeout(app.quit, 1000)
      return true

    case '--squirrel-obsolete':
      // This is called on the outgoing version of your app before
      // we update to the new version - it's the opposite of
      // --squirrel-updated

      app.quit()
      return true
  }
}
