import { contextBridge, remote } from 'electron'
contextBridge.exposeInMainWorld('electron', {
  showMeessage: (options: any):void => {
  }
})
declare global {
  interface Window {
    electron: Electron
  }
}

export interface Electron {
  showMeessage: (options) => void
}
