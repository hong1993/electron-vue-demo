export default [
  {
    path: '/',
    name: 'index',
    component: (): any => import('@/components/home/views/index.vue')
  },
  {
    path: '/setup',
    name: 'setup',
    component: (): any => import('@/components/home/views/setup.vue')
  }
]
