const Service = require('@vue/cli-service')
const mainConfig = require('../webpack.main.config')
const webpack = require('webpack')
const electron = require('electron')
const { spawn } = require('child_process')
const path = require('path')
let electronProcess = null
let manualRestart = false
function buildMain() {
  return new Promise((resolve, reject) => {
    mainConfig.mode = 'development'
    const mainCompiler = webpack(mainConfig)
    mainCompiler.hooks.watchRun.tapAsync('watch-run', (compilation, done) => {
      done()
    })
    mainCompiler.watch({}, (err, stats) => {
      if (electronProcess && electronProcess.kill) {
        manualRestart = true
        process.kill(electronProcess.pid)
        electronProcess = null
        runElectron()
        setTimeout(() => {
          manualRestart = false
        }, 5000)
      }
      resolve()
    })
  })
}
function runElectron () {
  var args = [
    path.join(__dirname, '../dist/electron/app.js')
  ]
  electronProcess = spawn(electron, args)
  electronProcess.on('close', () => {
    if (!manualRestart) {
      process.exit()
    }
  })
}
function buildRenderer() {
  return new Promise(resolve => {
    const service = new Service(process.env.VUE_CLI_CONTEXT || process.cwd())
    service.run('serve').then(() => {
      resolve()
    })
  })
}
function start() {
  Promise.all([buildRenderer(), buildMain()])
    .then(() => {
      runElectron()
    })
}
start()
