const electronInstaller = require('electron-winstaller')
const path = require('path')
async function start() {
  try {
    await electronInstaller.createWindowsInstaller({
      appDirectory: path.join(__dirname, '../build/iboot-vue3-ts-init-win32-x64'),
      outputDirectory: path.join(__dirname, '../build/iboot-vue3-ts-init-win32-x64/installer64'),
      exe: 'iboot-vue3-ts-init.exe',
      noMsi: true
    })
    console.log('It worked!')
  } catch (e) {
    console.log(`No dice: ${e.message}`)
  }
}
start()
